package com.codecream.listview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ItemListAdapter extends BaseAdapter {
    private Context context;
    public ItemListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return Inventory.getInstance().getCount();
    }

    @Override
    public Object getItem(int position) {
        return Inventory.getInstance().getItemAt(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Log.i("listitem", String.valueOf(position));
        View viewToReturn = null;
        if(convertView != null){
            viewToReturn = convertView;
            Log.i("listitem", "reusing convertview");
        }
        else{
            //construct new view
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            viewToReturn = layoutInflater.inflate(R.layout.itemlayout, null);
            Log.i("listitem", "creating new view");
        }

        Item item = (Item) this.getItem(position);
        String itemName = item.getItemName();
        TextView itemNameField = (TextView) viewToReturn.findViewById(R.id.itemName);
        itemNameField.setText(itemName);

        String itemPrice = item.getItemPrice();
        TextView itemPriceField = (TextView) viewToReturn.findViewById(R.id.itemPrice);
        itemPriceField.setText(itemPrice);
        return viewToReturn;
    }
}
