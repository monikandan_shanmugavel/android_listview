package com.codecream.listview;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    private List<Item> itemList;
    private static Inventory instance;

    private Inventory(){
        itemList = new ArrayList<>();
    }

    public static Inventory getInstance(){
        if(instance == null)
            instance = new Inventory();
        return instance;
    }

    public int getCount(){
        return itemList.size();
    }

    public void addItem(Item item){
        itemList.add(item);
    }

    public Item getItemAt(int position){
        return itemList.get(position);
    }
}
